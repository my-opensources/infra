# Infra : Network Squid role

The purpose of this role is to install and delete a squid service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                          | Default               | Description                                                 |
| ---------------------------- | --------------------- | ----------------------------------------------------------- |
| in_squid_action              | create                | The action to perform                                       |
| in_squid_port                | 3128                  | the port that will be used by squid for the proxy connexion |
| in_squid_package             | squid                 | Name of the proxy package                                   |
| in_squid_daemon              | squid                 | Name of the proxy daemon                                    |
| in_squid_conf                | /etc/squid/squid.conf | path of the configuration for the daemon                    |
| in_squid_open_egress_traffic | true                  | Should open all egress traffic or not ?                     |

## Dependencies

Depend only on `ansible.builtin`

## Example playbooks

### Install squid playbook

Install squid service default version.

```yaml
- name: Install squid service
  hosts: localhost
  roles:
    - role: startxfr.services.network_squid
      vars:
        in_squid_action: create
```

### Uninstall squid playbook

Uninstall squid service default version.

```yaml
- name: Uninstall network_squid service
  hosts: localhost
  roles:
    - role: startxfr.services.network_squid
      vars:
        in_squid_action: delete
```
