# Infra : Network BIND role

The purpose of this role is to create and delete a bind DNS service.
This role is part of the [STARTX infra ansible collection](https://galaxy.ansible.com/startxfr/infra).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`

## Role Variables

| Key                         | Default             | Description                                                                |
| --------------------------- | ------------------- | -------------------------------------------------------------------------- |
| in_bind_action              | create              | Action to perfom for the bind service                                      |
| in_bind_port                | 53                  | The port where the DNS server will listen                                  |
| in_bind_daemon              | bind                | Name of the proxy daemon                                                   |
| in_bind_package             | [bind, bind-utils]  | Name of the proxy package                                                  |
| in_bind_owner               | root                | The user owner of the named service that run the dns                       |
| in_bind_group               | root                | The group owner of the named service that run the dns                      |
| in_bind_forward_zone[].zone | forward.example.com | The zone that need to be configure with the server in the principal config |
| in_bind_reverse_zone[].zone | reverse.example.com | The zone that need to be configure with the server in the principal config |

## Dependencies

- `ansible.builtin` collection
- `startxfr.infra` collection. See [startx infra collection documentation](https://galaxy.ansible.com/startxfr/infra)
- `community.general` collection. See [community general collection documentation](https://galaxy.ansible.com/community/general)

## Example playbooks

### Create network_bind playbook

Create network_bind default configuration.

```yaml
- name: Create network_bind
  hosts: localhost
  roles:
    - role: startxfr.infra.network_bind
      vars:
        in_bind_action: "create"
```

### Delete network_bind playbook

Delete network_bind default configuration.

```yaml
- name: Delete network_bind
  hosts: localhost
  roles:
    - role: startxfr.infra.network_bind
      vars:
        in_bind_action: "delete"
```
