# STARTX Infra : AWS Codecommit role

Read the [startxfr.infra.aws_codecommit role documentation](https://startx-ansible-infra.readthedocs.io/en/latest/roles/aws_codecommit/)
for more information on how to use [STARTX infra ansible collection](https://galaxy.ansible.com/startxfr/sinfraxcm) aws_codecommit role.
