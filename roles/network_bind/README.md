# STARTX Infra : Network BIND role

Read the [startxfr.infra.network_bind role documentation](https://startx-ansible-infra.readthedocs.io/en/latest/roles/network_bind/)
for more information on how to use [STARTX infra ansible collection](https://galaxy.ansible.com/startxfr/sinfraxcm) network_bind role.
