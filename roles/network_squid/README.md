# STARTX Infra : Network Squid role

Read the [startxfr.infra.network_squid role documentation](https://startx-ansible-infra.readthedocs.io/en/latest/roles/network_squid/)
for more information on how to use [STARTX infra ansible collection](https://galaxy.ansible.com/startxfr/sinfraxcm) network_squid role.
