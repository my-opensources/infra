# STARTX Infrastructure Ansible collection

The purpose of this collection is to manage the creation-deletion of various infrastructure
services using aws or local backends.

Refer to [latest documentation for the `startxfr.infra` ansible collection](https://startx-ansible-infra.readthedocs.io)
for more informations on how to configure and use this top level role.

## Availables roles

- [awx_codecommit role](https://startx-ansible-infra.readthedocs.io/en/latest/roles/awx_codecommit)
- [network_openvpn role](https://startx-ansible-infra.readthedocs.io/en/latest/roles/network_openvpn)
- [network_squid role](https://startx-ansible-infra.readthedocs.io/en/latest/roles/network_squid)
- [network_bind role](https://startx-ansible-infra.readthedocs.io/en/latest/roles/network_bind)

## History

Full history for this collection is available on the [startx infra collection history](https://startx-ansible-infra.readthedocs.io/en/latest/history) page.
